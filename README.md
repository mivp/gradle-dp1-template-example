A gradle task calling the dp1 templating tool to create config files from a CAD model.

See `build.gradle` in the root of this repo for the actual task.

## Requirements

  * Installed Solidworks (2018)
  * Installed dp1 templating tool (see: https://gitlab.com/mivp/cadds)
     * and make sure it's path is in the %PATH%

## Run Task

```
> gradle createConfigFromCADModel
```
